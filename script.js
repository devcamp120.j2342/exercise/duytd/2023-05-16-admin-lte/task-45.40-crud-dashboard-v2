"use strict";
//REGION 1
const gBASE_URL = "http://203.171.20.210:8080/crud-api/users/";
// biến toàn cục để lưu trữ id user đang được delete và update mặc định bằng 0
var gUserId = 0;
// biến mảng hằng số chứa danh sách các tên thuộc tính
const gUSER_COLS = ["id", "firstname", "lastname", "country", "subject", "customerType", "registerStatus", "action"]
//biến mảng toàn cục đinh nghĩa chỉ số các cột tương ướng
const gUSER_ID_COL = 0;
const gFIRST_NAME_COL = 1;
const gLAST_NAME_COL = 2;
const gCOUNTRY_COL = 3;
const gSUBJECT_COL = 4;
const gCUSTOMER_TYPE_COL = 5;
const gREGISTER_STATUS_COL = 6;
const gACTION_COL = 7;
//khai báo dataTable và mapping column
var gUserTable = $("#user-table").DataTable({
    columns: [
        { data: gUSER_COLS[gUSER_ID_COL] },
        { data: gUSER_COLS[gFIRST_NAME_COL] },
        { data: gUSER_COLS[gLAST_NAME_COL] },
        { data: gUSER_COLS[gCOUNTRY_COL] },
        { data: gUSER_COLS[gSUBJECT_COL] },
        { data: gUSER_COLS[gCUSTOMER_TYPE_COL] },
        { data: gUSER_COLS[gREGISTER_STATUS_COL] },
        { data: gUSER_COLS[gACTION_COL] },
    ],
    columnDefs: [
        { //định nghĩa lại col action
            targets: gACTION_COL,
            defaultContent: `
                <img class="edit-user" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                <img class="delete-user" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                `
        }
    ]
})
//REGION 2
$(document).ready(function () {
    onPageLoading()
    //sự kiện thêm mới user
    $("#btn-thêm-user").on("click", function () {
        onBtnAddNewUserClick();
    })
    //gán sự kiện nút edit được ấn
    $("#user-table").on("click", ".edit-user", function () {
        onBtnEditClick(this);
    });
    //gán sự kiện nut delete được ấn
    $("#user-table").on("click", ".delete-user", function () {
        onBtnDeleteClick(this);
    });
    //gán sự cho nút Create User (trên modal)
    $("#btn-create-user").on("click", function () {
        onBtnCreateUserClick();
    })
    //gán sự kiện cho nút update user trên form (trên modal)
    $("#btn-update-user").on("click", function () {
        onBtnUpdateUserClick();
    });
    // gán sự kiện cho nút confirm delete (trên modal)
    $("#btn-confirm-delete-user").on("click", function () {
        onBtnConfirmDeleteUserClick()
    })
});
//REGION 3
function onPageLoading() {
    getAllUsers();
}
//hàm thự hiện sự kiện nút confirm delete được click
function onBtnConfirmDeleteUserClick() {
    console.log("nút được ấn");
    //b1 thu thập dữ liệu
    //b2 kiểm tra dữ liệu
    //b3 gọi api
    $.ajax({
        url: gBASE_URL + gUserId,
        type: "DELETE",
        success: function () {
            handleDeleteUserSuccess();
        },
        error: function (err) {
            console.log(err.status)
        }
    })
    //b4 xủ lý hiển thị
}
//hàm thự hiện sự kiện nút sửa được click
function onBtnEditClick(paramEvent) {
    console.log("nút edit được ấn");
    //lưu thông tin UserId đang được edit vào biến toàn cục
    gUserId = getUserIdFormBtn(paramEvent);
    console.log("id của user tương ứng là: " + gUserId);
    //hàm load dữ liệu lên form
    getUserById(gUserId);
}
//hàm thực hiện sự kiện nút delete được click
function onBtnDeleteClick(paramEvent) {
    console.log("nut delete được ấn");
    gUserId = getUserIdFormBtn(paramEvent);
    console.log("id của user tương ứng là: " + gUserId);
    //hiển thị modal
    $("#delete-confirm-modal").modal("show");

}
//hàm thực hiện sự kiện nút thêm mới user được ấn
function onBtnAddNewUserClick() {
    $("#create-user-modal").modal("show");
}
//hàm thự hiện sự kiện nút insert User (trên modal) được ấn
function onBtnCreateUserClick() {
    //khai báo đối tượng chứa user data
    var vCreateUser = {
        firstname: "",
        lastname: "",
        subject: "",
        country: "",
        customerType: "",
    }
    // b1 thu thập dữ liệu
    getInsertData(vCreateUser);
    // b2 kiểm tra dữ liệu
    var vValidate = validateUserData(vCreateUser);
    if (vValidate) {
        //b3 gọi api
        $.ajax({
            url: gBASE_URL,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vCreateUser),
            success: function (paramRes) {
                console.log(paramRes);
                // b4 xử lý hiển thị
                handlerInsertUserSuccess();
            },
            error: function (ajaxContext) {
                console.log(ajaxContext.status);
            }
        })
    }
}
//hàm thực hiện sự kiện nút update user trên form edit được click
function onBtnUpdateUserClick() {
    //khai báo đối tượng chứa data
    var vUserUpdate = {
        firstname: "",
        lastname: "",
        subject: "",
        country: "",
        customerType: "",
    }
    //b1 thu thập dữ liệu
    getUpdateUserData(vUserUpdate)
    //b2 kiểm tra dữ liệu
    var vIsCheck = validateUpdateUser(vUserUpdate)
    if (vIsCheck) {
        //b3 goi api
        $.ajax({
            url: gBASE_URL + gUserId,
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(vUserUpdate),
            success: function (res) {
                console.log(res);
                //b4: xử lý hiển thị
                resertUpdateUserForm(res);
            },
            error: function (error) {
                console.log(error.status);
            }
        });
    }

}
//REGION 4
//hàm xử lý hiển thị khi user được xóa thành công
function handleDeleteUserSuccess() {
    alert("Xóa user thành công!"); //thông báo xóa user thành công
    getAllUsers(); //load lại trang
    $("#delete-confirm-modal").modal("hide"); //ẩn modal
}
//hàm xử lý hiển thị khi update user thành công
function resertUpdateUserForm(paramObj) {
    alert("Thêm thông tin thành công"); //thông báo đã cập nhật thành công
    getAllUsers();  // load lại trang
    resertFormUpdateUser(); //xóa trắng form
    $("#update-user-modal").modal("hide");
}
//hàm xóa trắng form update user
function resertFormUpdateUser() {
    $("#input-update-firstname").val("");
    $("#input-update-lastname").val("");
    $("#input-update-subject").val("");
    $("#select-country").val("VN");
    $("#select-customertype").val("Standard");
}
//hàm thu thập dữ liệu trên form update user
function getUpdateUserData(paramUser) {
    paramUser.firstname = $("#input-update-firstname").val().trim();
    paramUser.lastname = $("#input-update-lastname").val().trim()
    paramUser.subject = $("#input-update-subject").val().trim();
    paramUser.country = $("#select-country").val();
    paramUser.customerType = $("#select-customertype").val();
}
//hàm kiểm tra dữ liệu update user trên form
function validateUpdateUser(paramUser) {
    if (paramUser.firstname == "") {
        alert("Bạn chưa nhập first name");
        return false;
    };
    if (paramUser.lastname == "") {
        alert("Bạn chưa nhập last name");
        return false;
    };
    return true;
}
//hàm gọi api lấy user qua id
function getUserById(paramUserId) {
    $.ajax({
        url: gBASE_URL + paramUserId,
        type: "GET",
        dataType: "json",
        success: function (response) {
            console.log(response)
            showDataToformUpdate(response);
        },
        error: function (xhr, status, error) {
            console.log(error)
        }
    })
    //hiển thị modal lên
    $("#update-user-modal").modal("show");
}
//hàm hiển thị data user lên form (modal) cần update
function showDataToformUpdate(paramRes) {
    $("#input-update-firstname").val(paramRes.firstname);
    $("#input-update-lastname").val(paramRes.lastname);
    $("#input-update-subject").val(paramRes.subject);
    $("#select-country").val(paramRes.country);
    $("#select-customertype").val(paramRes.customerType);
}
// xử lý hiển thị khi thêm user thành công
function handlerInsertUserSuccess() {
    alert("Thêm thông tin thành công");
    getAllUsers();
    resertCreateUserForm();
    $("#create-user-modal").modal("hide");
}
//hàm xóa trắng form create form
function resertCreateUserForm() {
    $("#inp-first-name").val("");
    $("#inp-last-name").val("");
    $("#select-country").val("0");
    $("#select-customertype").val("0");
    $("#inp-subject").val("");
}
//hàm thu thập dữ liệu trên form modal thêm user
function getInsertData(paramUser) {
    paramUser.firstname = $("#inp-first-name").val().trim();
    paramUser.lastname = $("#inp-last-name").val().trim();
    paramUser.subject = $("#inp-subject").val().trim();
    paramUser.country = $("#select-country").val();
    paramUser.customerType = $("#select-customertype").val();
}
//hàm kiểm tra dữ liệu trên (modal) thêm mới user
function validateUserData(paramUser) {
    if (paramUser.firstname == "") {
        alert("Bạn chưa nhập first name !!");
        return false;
    }
    if (paramUser.lastname == "") {
        alert("Bạn chưa nhập last name");
        return false;
    }
    if (paramUser.country == 0) {
        alert("Bạn chưa chon country");
        return false;
    }
    if (paramUser.customerType == 0) {
        alert("Bạn chưa chọn Customer type");
        return false;
    }
    if (paramUser.subject == "") {
        alert("Bạn chưa nhập subject");
        return false;
    }
    return true;
}

//hàm gọi api lấy data tất cả user
function getAllUsers() {
    $.ajax({
        url: "http://203.171.20.210:8080/crud-api/users/",
        type: "GET",
        success: function (response) {
            console.log(response);
            loadDataToTable(response);
        },
        error: function (ajaxContext) {
            console.log(ajaxContext.responseText);
        }
    })
}

//hàm load users to dataTable
function loadDataToTable(paramUser) {
    gUserTable.clear();
    gUserTable.rows.add(paramUser);
    gUserTable.draw();
}

//hàm dựa vào btn detail (edit or delete) xác định được id của user
function getUserIdFormBtn(paramBtn) {
    var vTableRow = $(paramBtn).parents("tr");
    var vUserRowData = gUserTable.row(vTableRow).data();
    return vUserRowData.id;
}